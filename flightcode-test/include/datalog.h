// datalog.h

#ifndef _DATALOG_h
#define _DATALOG_h
#include <math.h>
#include <string.h>
#include "stdio.h"

#define BUFFERSIZE 1028

#define NUMDATA 9 // How many data points we have (i.e. time, acceleration, etc.)

// Thinking we can add the data to this array while the loop is running
// and just clear it out after all the data is written
double data_array[NUMDATA];

FILE *dataFile;

//const char * fileName = "data.txt";  // Name of file

// All the names of the data points, which will work as column names
const char *headers[NUMDATA] = {
    "Time (sec)",
    "PID Output",
    "Acceleration (ft/s^2)",
    "Filtered Acceleration",
    "Velocity (ft/s)",
    "Filtered Velocity",
    "Altitude (ft)",
    "Filtered Altitude",
    "Projected Altitude (ft)",
};

// Set up text file with headers and what not.
void initialize_file()
{
    dataFile = fopen("data.txt", "w");
    // printing the data file
    if (dataFile)
    {
        char header_buffer[BUFFERSIZE];
        for (int i = 0; i < NUMDATA; i++)
        {
            if (i == 0)
            {
                sprintf(header_buffer, "%s\t", headers[i]);
            }
            else if (i < NUMDATA - 1)
            {
                sprintf(header_buffer + strlen(header_buffer), "%s\t", headers[i]);
            }
            else if (i == NUMDATA - 1)
            {
                sprintf(header_buffer + strlen(header_buffer), "%s\n", headers[i]);
            }
        }
        fputs(header_buffer, dataFile);
    }
}

// Get the length of the header for each column, so we can format the data
int header_len(const char *header)
{
    return strlen(header);
}

// Number of whole digits for each data point
int data_len(double data)
{
    int n = (int)data;
    if (n == 0)
        return 1;
    // if data is negative, make room for negative sign
    else if (n < 0)
        return floor(log10(fabs(n))) + 2;
    // else return number of whole digits
    else
        return floor(log10(fabs(n))) + 1;
}

// Load the current iteration's data into an array to print
// void push_data(double dataArray[NUMDATA]);

// Clear the current iteration's data so we can load the next iteration
void pop_data()
{
    memset(data_array, 0, sizeof(data_array));
}

void close_file()
{
    fclose(dataFile);
}

// pretty print text to file
void print_data()
{
    if (dataFile)
    {
        // Length of data array
        int arrLength = sizeof(data_array) / sizeof(data_array[0]);
        char data_buffer[BUFFERSIZE]; // load each formatted data point into this buffer, then write to the file

        for (int i = 0; i < arrLength; i++)
        {
            // The amount of indenting per column so data is indented correctly
            // Subtracting 3 here because we're printing the data to two decimal places
            // so we need space for the two decimal places and the period
            int indent = header_len(headers[i]) - data_len(data_array[i]) - 3;
            // Print data tab delimited, unless it's the last data point, then print a new line
            if (i == 0)
            {
                sprintf(data_buffer, "%*s%.2f\t", indent, "", data_array[i]);
            }
            else if (i < arrLength - 1)
            {
                sprintf(data_buffer + strlen(data_buffer), "%*s%.2f\t", indent, "", data_array[i]);
            }
            else if (i == arrLength - 1)
            {
                sprintf(data_buffer + strlen(data_buffer), "%*s%.2f\n", indent, "", data_array[i]);
            }
        }
        fputs(data_buffer, dataFile);
    }
}

#endif
