// kalman.h

#ifndef FILTER_H
#define FILTER_H

#define ARRLEN 15

class RollingAverage
{
  private:
	int arr_len = ARRLEN;
	int index;
	double data_array[ARRLEN] = {0};

	// Load current iteration data to array, start at beginning if at end
	void push(double data)
	{
		if (index < arr_len - 1)
		{
			data_array[index] = data;
		}
		else if (index == (arr_len - 1))
		{
			data_array[index] = data;
			index = 0;
		}
		index += 1;
	}

  public:
	// Initializing
	RollingAverage()
	{
		index = 0;
	}

	// Get the averaged value
	double output(double input)
	{
		push(input);
		double sum = 0;
		int k = 0;
		for (double num : data_array)
		{
			if (num != 0)
			{
				sum += num;
				k += 1;
			}
		}
		return sum / k; // Return the average
	}
};

// Using a class here because some sensors are noisier than others,
// and the Kalman filter depends on each sensor's value to work.
class Kalman
{
  private:
	double K_k;		// Kalman gain
	double P_k;		// Error covariance
	double P_t;		// Combined covariance
	double R;		// Covariance, or noise
	double Q;		// Environmental uncertainty
	double x_k;		// Altitude calculation
	double x_t;		// Combined altitude
	double v_k;		// Velocity calculation
	double v_t;		// Combined velocity
	unsigned int k; // Number of iterations
	double dt;		// Change in time

	double a_t;		// Combined acceleration
	double R_a;		// Covariance, or noise
	double P_k_1_a; // Error covariance from the previous iteration
	double a_k_1;

	// Private method that saves the current variables to be used in the next iteration.
	void update_x(double z, double u, double a)
	{
		x_k += (v_k * dt) + (0.5 * a * (dt * dt)); // Calculate the new altitude
		v_k += (a * dt);						   // Calculate the new velocity
		P_k += Q;								   // Compute the new covariance
		K_k = P_k / (P_k + R);					   // Compute the Kalman gain for this iteration
		x_t = x_k + (K_k * (z - x_k));			   // Calculate the new filtered altitude output
		v_t = v_k + (K_k * (u - v_k));			   // Calculate the new filtered velocity output
		P_t = (1 - K_k) * P_k;					   // Calculate new error covariance
		P_k = P_t;								   // Set error covariance for next iteration
		x_k = x_t;								   // Set the old output for the next iteration
		k += 1;
	}

	void update_a(double a_k)
	{
		K_k = P_k / (P_k + R);				 // Compute the Kalman gain for this iteration.
		a_t = a_k_1 + (K_k * (a_k - a_k_1)); // Calculate the new filtered output
		P_t = (1 - K_k) * P_k;				 // Calculate new error covariance
		P_k = P_t;							 // Set error covariance for next iteration
		a_k_1 = a_t;
	}

  public:
	typedef struct
	{
		double alt, veloc, accel;
	} State;

	// Constructor
	Kalman(double sensor_noise,
		   double covariance,
		   double environ_uncert,
		   double time_step,
		   double init_alt,
		   double init_veloc,
		   double init_accel)
	{
		R = sensor_noise;
		Q = environ_uncert;
		P_k = covariance;
		x_k = init_alt;
		v_k = init_veloc;
		a_k_1 = init_accel;
		dt = time_step;
	}

	Kalman(double sensor_noise, double covariance)
	{
		this->R_a = sensor_noise;
		this->a_k_1 = -32.174;
		this->P_k_1_a = covariance;
	}

	// Use this method when we want to get the filtered output
	State output(double z, double u, double a)
	{
		update_x(z, u, a);
		update_a(a);
		State state;
		state.accel = a_t;
		state.veloc = v_t;
		state.alt = x_t;
		return state;
	}
};

#endif // FILTER_H
