#ifndef VEHICLE_H
#define VEHICLE_H
#include "math.h"
// physics.h keeps as much of the simulation stuff outside of
// the flight source code so the flight code is kept the same when possible
#include "physics.h"

// State of the vehicle
/* Global vars */

namespace PID
{
double P;
double I;
double D;
double P_0;
} // namespace PID

/* ALL THE CONSTANTS THE CONTROLLER NEEDS TO FUNCTION */
/* KP, KI and KD gathered from Simulink simulations */
const double GAINFACTOR = 0.01;
const double KP = 10 * GAINFACTOR; // Proportional gain.
const double KI = 1 * GAINFACTOR;  // Integral gain.
const double KD = 50 * GAINFACTOR; // Derivative gain.
const int I_MAX = 100;             // Prevents integral windup.
const double SETPOINT = 5280;      // Target altitude in feet.

const double MAX = 1; // Maximum input to motor.
const double MIN = 0; // Minimum input to motor.

const double METERTOFEET = 3.280839895;

// Using an enum since it takes less space than strings and is clearer than just numbers
// PID::I think it's clearer than what we did last year, where we had loops inside the main loop.
enum Runmode
{
    idle,   // Rocket is sitting on launchpad
    launch, // Motor is ignited and burning (have to wait for burnout)
    coast,  // Motor is burnt out, this is where the fairings will do their thing
    descent // Rocket is now descending - retract fairings and power down
};

// This is our max altitude, which is what the pid controller uses to calculate
// the position of the plates. You can find the equations used here:
// https://www.grc.nasa.gov/www/k-12/airplane/flteqs.html. Actually simplified
// the equation so it's a bit shorter.
double projected_altitude(double veloc, double accel, double currentAlt)
{
    double veloc_sqr = veloc * veloc;
    return (-veloc_sqr / (2 * (accel + GRAV))) * log(-accel / GRAV) + currentAlt;
}

// emulate the amount of time it takes the plates to move, currently they
// take 1.5 seconds to move from retracted to extended

// The actual controller.
double pid(double position, double proj_alt)
{

    PID::P = proj_alt - SETPOINT; // Proportional term.

    PID::I += PID::P * TIMESTEP;

    PID::D = (PID::P - PID::P_0) / 2; // Derivative term.

    // diffOutput is the change in plate position, so we add it to the old position.
    double diffOutput = (PID::P * KP) + (PID::I * KI) + (PID::D * KD);

    // This block saves the variables in this loop so it can be used in the next.
    PID::P_0 = PID::P;

    // updating the position of the plates.
    position += diffOutput;

    // These prevent the new position of the plates from going over their max and min values.
    if (position > MAX)
        position = MAX;
    else if (position < MIN)
        position = MIN;

    return position;
}

double get_accel(double veloc, double alt, double input)
{
    return acceleration(veloc, alt, input);
}

double get_veloc(double accel, double veloc)
{
    return velocity(accel, veloc);
}

double get_alt(double veloc, double alt)
{
    return altitude(veloc, alt);
}

/* 
 * The reason this function is here is to check if the rocket is actually launching, 
 * or the motor burned out, etc
 */
bool verify_switch(bool (*condition)(), double start_time, double curr_time)
{
    bool switch_state;
    while (1)
    {
        if (condition())
        {
            if (fabs(curr_time - start_time) >= 0.5)
            {
                switch_state = true; // Actually doing the thing
                break;
            }
        }
        else if (!condition())
        {
            switch_state = false; // Actually did not do the thing
            break;
        }
    }
    return switch_state;
}

#endif // VEHICLE_H
