#ifndef PHYSICS_H
#define PHYSICS_H
#include <stdlib.h>
#include <time.h>

const double TIMESTEP = 0.1;
const double GRAV = 32.1737; // Gravity constant in english units.

/* Generate sensor noise */
double gen_noise(int min, int max)
{
    int span = max - min;
    int num_range = rand() % (span * 100) + (min * 100);
    return (double)num_range / 100;
}

/* Simulates the plates moving (since they can't go all the way out instantly, it takes time) */
double emulate_plate_movement(double desired_pos, double current_pos)
{
    // the max speed represents the amount of time the plates need to go from fully retracted to fully
    // extended, which is about 1.75 seconds if I'm not mistaken
    double max_speed = 1.0 / 17.5;
    double move_speed = 1.0 / 50.0;
    static double speed; // static variables in functions are conserved each time the func is called
    double error = desired_pos - current_pos;
    if (error != 0)
    {
        speed += move_speed * (error / abs(error));
        if (speed > max_speed)
            speed = max_speed;
        else if (speed < -max_speed)
            speed = -max_speed;
        current_pos += speed;
    }
    if (current_pos < 0.0)
        current_pos = 0.0;
    else if (current_pos > 1.0)
        current_pos = 1.0;
    return current_pos;
}

/* Simulate the reduction in density as the vehicle ascends */
double get_density(double alt)
{
    double temp = 59 - (0.00356 * alt) + 459.67;
    double press = 2116 * pow(temp / 518.6, 5.256);
    double density = (press / (1718 * temp));
    // convert density from slugs/ft^3 to lbs/ft^3
    return GRAV * density;
}

/* Acceleration of the vehicle */
double acceleration(double veloc, double alt, double input)
{
    double veloc_sqr = veloc * veloc;
    double area = 0.1935;
    double rho = get_density(alt);
    double c_d = 0.55 + (0.7 * input);
    double mass = 40;
    return -GRAV - ((c_d * area * rho * veloc_sqr) / (2 * mass));
}

/* Velocity of the vehicle */
double velocity(double accel, double veloc)
{
    return veloc += accel * TIMESTEP;
}

/* Altitude of the vehicle */
double altitude(double veloc, double altitude)
{
    return altitude += veloc * TIMESTEP;
}

#endif // PHYSICS_H