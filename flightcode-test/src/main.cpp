
#include "filter.h"
#include "vehicle.h"
#include "datalog.h"
#include "time.h"

int main()
{
    srand(time(NULL));
    Runmode MODE = coast;
    double time = 0;
    double alt = 1000;
    double veloc = 700;
    double accel = get_accel(veloc, alt, 0);
    double plate_pos = 0;
    double desired_pos = 0;
    double proj_alt = projected_altitude(veloc, accel, alt);

    Kalman filter = Kalman(3, 1, 1, TIMESTEP, alt, veloc, accel);
    Kalman::State filter_vals = filter.output(alt, veloc, accel);
    // Open up the data file and print the header
    initialize_file();

    while (true)
    {
        //Sitting on the launch pad.
        // if (MODE == idle)
        // {
        //     // Condition in this case is is the acceleration greater than 0
        //     // This will hopefully switch the state to launch if the acceleration doesn't trigger it
        //     // for some reason
        //     if (fabs(alt > 50) {
        //         bool condition()
        //         {
        //             return alt > 50;
        //         }
        //         bool switch_state = verify_switch(condition);
        //         if (switch_state)
        //         {
        //             MODE = launch;
        //         }
        //     }
        // }

        // // Motor is burning, have to wait until burn out.
        // else if (MODE == launch)
        // {
        //     if (accel < 0)
        //     {
        //         // Same thing as above
        //         bool condition()
        //         {
        //             return accel < 0;
        //         };
        //         if (verify_switch(condition))
        //             MODE = coast;
        //     }
        // }

        if (MODE == coast)
        {
            double alt_noise = gen_noise(-100, 100);
            double veloc_noise = gen_noise(-25, 25);
            double accel_noise = gen_noise(-3, 3);
            // Load up all the data points
            data_array[0] = time;
            data_array[1] = plate_pos;
            data_array[2] = accel + accel_noise;
            data_array[3] = filter_vals.accel;
            data_array[4] = veloc + veloc_noise;
            data_array[5] = filter_vals.veloc;
            data_array[6] = alt + alt_noise;
            data_array[7] = filter_vals.alt;
            data_array[8] = proj_alt;

            print_data(); // Write data to file
            pop_data();   // Clear data array

            proj_alt = projected_altitude(filter_vals.veloc, filter_vals.accel, filter_vals.alt);
            desired_pos = pid(plate_pos, proj_alt);
            plate_pos = emulate_plate_movement(desired_pos, plate_pos);
            alt = get_alt(veloc, alt);
            veloc = get_veloc(accel, veloc);
            accel = get_accel(veloc, alt, plate_pos);
            filter_vals = filter.output(alt + alt_noise, veloc + veloc_noise, accel + accel_noise);

            // Reached apogee, stop doing stuff now
            if (veloc < 0)
            {
                MODE = descent;
            }
        }

        else if (MODE == descent)
        {
            // Retract the fairings, close the data file, and power down after all that's done.
            if (dataFile)
            {
                close_file();
            }
            break;
        }
        time += TIMESTEP;
    }
}
