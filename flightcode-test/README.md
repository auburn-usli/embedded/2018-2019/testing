# Flightcode Testing

## Overview

The code in this directory simulates the vehicle in flight to test the flight code itself and make sure it works as intended. In this file below is a brief overview of the code and how to work with it if you are new to working with C/C++ code. This code in particular is compiled using a C++ compiler rather than Arduino, so you will need to install that if you haven't already. If you already know how to work with C/C++ and know your way around a terminal then you can of course skip this. However, this README assumes you have no prior experience working within Unix environments or C/C++, since not everyone on this team is in Computer Science, Software Engineering, etc. If you have any issues getting this stuff to work get in touch with me and I can help walk you through it.

## Files
In this directory there are four header files and one main file which can be found in _include/_ and _src/_ respectively:

1. _vehicle.h_ 
   
   Contains pid controller function, constants for the controller and physics (gravity, etc.), and contains functions that return values from sensors. In the testing version, since we can't use the sensors themselves, _vehicle.h_ includes _physics.h_, which performs the physics calculations and returns the velocity, altitude, and acceleration of the vehicle, which _vehicle.h_ can return to the main script. Since we want to minimize the amount of changes to the actual flight code, _get_accel()_, _get_veloc()_, and _get_alt()_ return data from _physics.h_, instead of just using the functions in _physics.h_.

2. _datalog.h_ 
   
   Takes values from the main script, formats all of them into a string and prints it to a file. This makes it easy to read and plot with Python (or MATLAB) and looks like an excel table.

3. _filter.h_
   
   The data from the sensors are noisy they need to be filtered, or else the pid controller will be inaccurate. The filtering algorithms are implemented in this file.

4. _physics.h_
   
   This header file will not be present in the actual flight code. Its purpose is to simulate the physics of the vehicle in flight and return those values to the main script. The idea is to keep as much of the flight code intact as possible, and use the functions implemented in _physics.h_ to return data from the functions in _vehicle.h_ that would normally read data from sensors in flight.

The main source file in the _src/_ directory is a C++ file rather than an Arduino file. Arduino code doesn't run on regular PCs easily (as far as I know), so the code is compiled using a regular C++ compiler for convenience. In the future we should work on writing a version of this that will run on the Blue Pill, but for the time being we can use this script to test some of the filtering algorithms, equations, etc.

If you look in the _util/_ directory you will find _plot.py_. You can use this to plot the results from the data file. I'll get around to writing a command line interface for it to make it easier to use, but it's functional for now. It requires _pandas_ and _matplotlib_. If you don't have them installed pull up a terminal and run this command:

```
pip install matplotlib pandas
```

Assuming you're still in the root directory, you can run this command and it will plot the results of _data.txt_ if it exists:

```
python util/plot.py
```

In the root directory you'll also see a file called _Makefile_ and _.gitignore_. _Makefile_ is used to help you compile your code, but don't worry about what's in it, you won't need to edit it. _.gitignore_ is a file that is used by _git_. It doesn't affect the code itself, but it's used by _git_ to ignore files that you write in it, so it won't track them.


## Compiling

This code is compiled using _g++_ and _make_. If you're not familiar with _make_ or compiling C++ code, _make_ is a program which helps automate the compilation process since compiling C++ normally is a pain and takes a while. The problem is you can only use _make_ on Unix machines (so Linux or Mac). If you're using Windows I recommend installing _Windows Subsystem for Linux_, because working with C++ on Windows is awful. It's really easy to find the instructions online, just make sure you use _Powershell_ instead of _cmd.exe_ or else it won't install properly, you'll see what I mean when you get there. I also recommend installing Ubuntu instead of any of the other distros because it's the most painless to set up.

If you're using Linux, make sure you have the packages installed. If you're not sure how to do that, open up a terminal and run this command:

```
sudo apt install g++ make
```

Which will install _g++_ and _make_ (remember they're two different programs), or update them if they're already installed. This also applies to you if you're using _WSL_. If you're using Windows, you can open up a terminal by searching for the _WSL_ app you installed (for example, if you installed Ubuntu, just search for Ubuntu and run that). It will ask for your password first (because you ran _sudo_ first, which is like running a program as administrator on Windows), so enter that first. It will look like nothing is happening when you are entering your password, but this is because it hides the characters too, so don't panic. Hit enter and it will run. 

If you're using a Mac, I would install _Homebrew_ first. It's a package manager like _apt_ but for Mac. Google _mac homebrew_ and follow the instructions. Once that's done, open up a terminal (it's in _Applications_) and run:

```
brew install make
```

To get _g++_ you need to install _Xcode_ from the App Store. Yes it's not ideal but it's the only way to get any C/C++ compiler on Macs. After that you should be good to go, you can use the same commands in the terminal as Linux.

Once that is done, you will be able to compile the program using the following command:

```
make
```

Which will create the executable in _bin/_ called _main_. To run this, you could run it directly using by running this command:

```
./bin/main
```

But I recommend running this command instead:

```
make run
```

This command will remove the old executable, recompile it, and run it in one command. You need to do this every time you make a change since the changes aren't added to the old executable. You need to delete the old one and recompile it. 

If you just want to delete the executable run:

```
make clean
```
And it will delete it.

Lastly, if you want to want to cycle through all the commands, you can run:

```
make plot
```

This will delete the old executable, recompile and run it, and plot the results in one command.