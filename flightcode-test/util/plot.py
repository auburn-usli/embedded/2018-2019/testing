from pandas import read_csv
import matplotlib.pyplot as plt

file = 'data.txt'
data = read_csv(file, delimiter='\t')
col_names = list(data.columns.values)
data.plot(x='Time (sec)', y=[col_names[2], col_names[3]], kind='line')
data.plot(x='Time (sec)', y=[col_names[4], col_names[5]], kind='line')
data.plot(x='Time (sec)', y=[col_names[6], col_names[7]], kind='line')
data.plot(x='Time (sec)', y=[col_names[1]], kind='line')
plt.show()
